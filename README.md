# Treinamento descomplicando o ansible

Projeto do treinamento para instação de um cluster Kubernetes Utilizando o Ansible + AWS.

# Fases do projeto

- Provisioning -> Criar as instâncias/vms para o nosso cluster.
- Install_K8s -> Criação do cluster, etc.
- Deploy_app -> Deploy de uma aplicação de exemplo
- Extra -> Segredo


##

```
export ANSIBLE_HOST_KEY_CHECKING=false
vim /etc/ansibl/ansible.cfg
vim ~/.ansible.cfg

[defaults]
host_key_checking=false

```